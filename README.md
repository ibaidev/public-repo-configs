# Public repository configurations

1. Install dependencies.

    ```bash
    apt update && apt install -y git curl
    ```

2. Init script.

    ```bash
    curl -s https://gitlab.com/ibaidev/gitness/-/raw/main/gitness.sh | sh -s restore https://gitlab.com/ibaidev/public-repo-configs.git
    ```
